;;; This file is for all the functions relating to dealing
;;; with the genome. Including encoding, decoding and printing

;; Convert the genotype to it's float value
(defun get-phenotype (lst &key ((:base base) *radix-value*) ((:rpoint rp) *radix-offset*))
	(let ((out 0.0))
		(loop for i from 0 below (length lst)
			do (setf out (+ out (* (expt base (- i rp))
				(nth i (reverse lst))))))
		out))

;; Prett print function for final output
(defun get-pretty (lst)
	(let ((pheno (get-phenotype lst)) (out ""))
		(loop for i from 0 below (length lst)
			do (progn
					(if (= i *radix-offset*) (setf out (concatenate 'string out ".")))
					(setf out (concatenate 'string out (write-to-string (nth i lst))))))
		out))
