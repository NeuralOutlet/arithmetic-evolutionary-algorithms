;;; ========================================================= ;;;
;;; Genetic Algorithm (IEEE, Binary, & Phinary)               ;;;
;;; ========================================================= ;;;

;;  This is a Genetic Algorithm for finding the indeterminant
;;  in a given polynomial.
;;
;;  Example:
;;  Solving for x where x^2 - x - 1 = 0


;;; Test program 
(defvar *phi* (/ (+ 1 (sqrt 5.0d0)) 2))
(defvar *radix-offset* 8)
(defvar *radix-value* *phi*)

;; Load in aga files
(load "genetic-operations.lsp")
(load "aga-genome-parser.lsp")
(load "population.lsp")

;; The polynomial to be tested
(defun poly (x)
	(- (expt x 2) x 1))

;; The fitness function
(defun get-fitness (pheno)
	(abs (poly pheno)))

;; Good old demo func
(defun demo (lst)
	(get-fitness (get-phenotype lst)))

;; Example with 1K generations of 100 64bit genomes
(defvar lowest most-positive-double-float)
(defvar bestlst '())
(defun side-effect (val lst gen)
	(let ((solution (get-phenotype lst)))
		(setf lowest val)
		(setf bestlst lst)
		(format nil "~C ~a: ~a ==> ~a = ~a ~C" #\linefeed gen lst solution lowest #\linefeed)))

(defvar *test* (generate-pop 100 16))
(defvar *population* nil)
(defvar *crossed* nil)
(defvar *mutated* nil)
(defvar *solutions* nil)
(defvar *last-best* "")

(defun generate-once (gen-count)
	(setf *population* (selection *test*))
	(setf *crossed* (pop-crossover *population* 100))
	(setf *mutated* (pop-mutate *crossed*))
	(setf *solutions* (loop for genome in *mutated*
		do (let ((fitness (demo genome)))
			(if (<= fitness lowest) (setf *last-best* (side-effect fitness genome gen-count))))
		when (eq lowest 0) collect (get-phenotype genome)))
	(setf *test* *mutated*)
	*last-best*)


;;;; ------------------------------------------ ;;;;
;;; Main program to be run if using command line ;;;
;;;; ------------------------------------------ ;;;;

;; Check to see if this is a gui invocation
(defvar *from-gui* (if (boundp '*from-gui*) *from-gui* nil))

;; The command line version
(if (equal *from-gui* "--gui") nil
	(progn
		(loop for i from 0 below 100
		until (eq lowest 0.0)
		do (format t "~a" (generate-once i)))

		(format t "~C" #\linefeed )
		(format t "Best solution: ~a" (get-pretty bestlst))
		(format t "~C" #\linefeed )))
