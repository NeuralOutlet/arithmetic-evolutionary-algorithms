;;; ================================ ;;;
;;; Mutation and Crossover Functions ;;;
;;; ================================ ;;;

;; Nice and random
(setf *random-state* (make-random-state t))

;; To be set in main when finished
(defvar *mutation-rate* 0.4)

;; Simple Flip-Bit method
(defun mutate (bit-list &optional (bit-count 1))
	(let ((counter bit-count) val genome)
		(setf val (random 1.0))
		(setf genome (copy-list bit-list))
		(loop for i from 0 below bit-count
			do (let ((gene (random (length genome))))
				(if (< val *mutation-rate*) (setf (nth gene genome) 
					(if (eql (nth gene genome) 0) 1 0)))))
		genome))
	
;; Arithmutaton method
(defun arithmutate (bit-list &optional (radix 2))
	bit-list)
	
;; Simple N-point crossover
;; Defaults as 2-point crossover
(defun crossover (one two &optional (points 2))
	(let (point-list be-one)
		(setf point-list (loop for i from 0 below points collect (random (length one))))
		(setf be-one t)
		(loop for i from 0 below (length one)
			collect (if be-one (nth i one) (nth i two))
			when (integerp (find (1+ i) point-list)) do (setf be-one (not be-one)))))
