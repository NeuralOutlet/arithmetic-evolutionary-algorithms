;;; ========================================================= ;;;
;;; Slowly developing a Phinary Genetic Programming Algorithm ;;;
;;; ========================================================= ;;;

;;  This is Genetic Programming for finding the root polynomial
;;  of some value. The value, x, is plugged into a randomly
;;  generated equation which then mutates towards to solution.
;;
;;  A polynomial is stored in the form of a binary genotype.
;;  Mutation can effect the entire equation, fitness is how
;;  close the answer is to 0. For example:
;;
;;  110100001000011110011001
;;  110 | 100 | 001 | 000 | 011 | 110 | 011 | 001
;;   x     ^     1     0     -     x     -     1
;;  
;; read as a binary sum the phenotype becomes
;;   x^2 - x - 1 = 0

;; Load in all the functions
(load "genetic-operations.lsp")
(load "agp-genome-parser.lsp")
(load "population.lsp")

;;; Test program 

;; Roughly our solution is
(defvar x (/ (+ 1 (sqrt 5.0d0)) 2))

;; The example genotype for x^2 - x - 1:
(defvar *example* '(1 0 1 1 0 0 0 0 1 0 0 0 0 1 1 1 0 1 0 1 1 0 0 1))

(defun demo (lst)
	(get-fitness (get-phenotype lst)))


;; Example with 100 generations of 100 64bit genomes
(defvar answers '())
(defvar solution '(? ? ? ?))
(defvar lowest most-positive-double-float)

(defun side-effect (val lst gen)
	(setf solution (get-phenotype lst))
	(setf lowest val)
	(format t "~C ~a ==> ~a = ~a ~C" #\linefeed gen solution lowest #\linefeed)
	(if (and (eq lowest 0.0d0) (null (find solution answers :test #'equal)))
		(setf answers (append answers solution))))

(defvar test (generate-pop 100 64))
(let (popu crossed mutated solutions)
	(loop for i from 0 below 100
		until (eq lowest 0.0)
		do (progn
			(setf popu (selection test))
			(setf crossed (pop-crossover popu 100))
			(setf mutated (pop-mutate crossed))
			(setf solutions (loop for genome in mutated
				do (let ((fitness (demo genome)))
					(if (<= fitness lowest) (side-effect fitness genome i)))
				when (eq lowest 0) collect (get-phenotype genome)))
			(setf test mutated))))

(format t "~C" #\linefeed )
(format t "Best solution: ~a = ~a" (pretty-pheno solution) lowest)
(format t "~C" #\linefeed )

