;; For the sake of loveliness
(defun ^ (a b) (expt a b))

;; Global variable, look-up list
;; the possible values for gene trios
(defvar *genes* '((0 (0 0 0))
                  (1 (0 0 1))
                  (+ (0 1 0))
                  (- (0 1 1))
                  (^ (1 0 0))
                  (x (1 0 1))
                  (1 (1 1 0))
                  (0 (1 1 1))))

;; Roughly our solution is
(defvar x (/ (+ 1 (sqrt 5.0d0)) 2))

;; The example genotype for x^2 - x - 1:
(defvar *example* '(1 0 1 1 0 0 0 0 1 0 0 0 0 1 1 1 0 1 0 1 1 0 0 1))

;; Function for taking a gene (3-tuple) 
;; and returning the key value in *genes*
(defun get-gene (gene)
	(car (rassoc gene *genes* :key #'car :test #'equal)))

;; Function for converting a genotype into
;; a list representing a binary sum
(defun get-binary-sum (genotype)
	(remove nil (loop for (a b c) on genotype by #'cdddr
		collect (get-gene (list a b c)))))

;; The delimiters needed are the variable x 
;; and the operators: +, -, ^. Multiplication
;; is implied by position. 
(defun is-delim (char)
	(let ((delims (list '+ '- 'x '^)))
		(if (find char delims)
			t)))

;; Function to convert binary list into decimal value
(defun from-base (num-lst)
	(let ((val 0) (counter 0))
		(loop for d in (reverse num-lst) do
			(setf val (+ val (* d (expt 2 counter))))
			(incf counter))
		val))

;; Function to turn '(a (b c) d) into '(a b c d)
(defun flatten (lst)
	(cond ((null lst) nil)
	((atom lst) (list lst))
	(t (loop for x in lst appending (flatten x)))))

(defun is-value (x)
	(if (or (eq x 'x) (integerp x)) t nil))

;; Function for implicit multiplication
;; Adds '*' between neighbouring values
(defun imply-multi (lst)
	(let (out)
		; buffer the list with head and tail nils
		(setf lst (cons nil (reverse 
			(cons nil (reverse lst)))))

		; loop through taking into account 
		; the previous and next values
		(setf out (loop for i from 1 below (1- (length lst))
		when (and (is-value (nth i lst)) (not (eq (nth i lst) 0))
					(is-value (nth (1- i) lst)) (not (eq (nth (1- i) lst) 0)))
			collect (list '* (nth i lst)) ; implicit multiplication
		else collect (nth i lst)))

	; flatten list
	; PREVIOUSLY: (setf out (flatten out)))
		(flatten out)))

;; Function to swap any ^ preceding 'x with *
;; In keeping with the rules that polynomial
;; terms can only have integer exponents
(defun diophant (lst)
	(let (out)
		; buffer the list with head and tail nils
		(setf lst (cons nil (reverse 
			(cons nil (reverse lst)))))

		; loop through taking into account 
		; the previous and next values
		(setf out (loop for i from 1 below (1- (length lst))
		when (and (eq (nth i lst) '^) (or 
				(eq (nth (1+ i) lst) 'x)
				(< (nth (1+ i) lst) 2)
				(> (nth (1+ i) lst) 10)))
			collect '* ; implicit multiplication
		else collect (nth i lst)))))

;; Function for removing 'junk-data' that doesn't
;; have a meaningful use in the equation
(defun clean-list (lst)
	(let (out)
		; buffer the list with head and tail nils
		(setf lst (cons nil (reverse 
			(cons nil (reverse lst)))))

		; loop through taking into account 
		; the previous and next values
		(setf out (loop for i from 1 below (1- (length lst))
			when (cond
				((and (or (null (nth (1- i) lst)) (null (nth (1+ i) lst))) (not (is-value (nth i lst)))) nil)
				((and (is-value (nth i lst)) (not (eq (nth i lst) 0))) t)
				((and (not (is-value (nth i lst))) (is-value (nth (1- i) lst)) (not (eq (nth (1- i) lst) 0))) t)
				((and (is-value (nth i lst)) (not (eq (nth i lst) 0)) (eq (nth (1- i) lst) 0)) t)
				 (t nil))
			collect (nth i lst)))

		; Remove trailing operands
		(if (not (is-value (car (reverse out))))
			(reverse (cdr (reverse out))) out)))

;; Function for getting a valid equation list
;; Discards extra operands, removes free zeros
;; and adds multiplication where needed.
(defun validate (lst)
	(imply-multi (diophant (clean-list lst))))


;; Function for interpreting the binary sum list
;; Interpret a list of numbers and operators as 
;; single binary numbers and operators
;; output as decimal
(defun get-phenotype (lst)
	(let ((bin-lst (get-binary-sum lst)) temp out)
		(setf temp (loop for i from 0 below (length bin-lst)
			if (is-delim (nth i bin-lst)) collect (nth i bin-lst)
			else collect (let ((a 'b))
					(setf out (loop for i from i below (length bin-lst)
						collect (nth i bin-lst) until (not (integerp (nth (1+ i) bin-lst)))))
					(setf i (+ i (1-(length out))))
					out)))

	(setf temp (loop for x in temp
		if (is-delim x) collect x
		else collect (from-base x)))
	(setf temp (validate temp))
	(bodmas temp)))

;; A way of evaluating the 'x's in the list
;; There's probably an obvious solution to this
(defun get-nth (n lst)
	(let ((out (nth n lst)))
		(if (not (eq out 'x)) out x)))
		
;; Solve using the value 2 to see 
;; if it is basically 0 = 0
(defun dummy-solve (lst)
	(let ((x 2)) 
		(if (equal (evaluate lst) 0) t nil)))

;; Returns poly-fitness which is
;; how ordered the powers are
(defun check-poly (lst)
	(let (buflst poly)
		; flatten and buffer with nil
		(setf buflst (reverse (cons nil (reverse (flatten lst)))))
		(setf poly (loop for i from 0 below (length buflst)
			when (and (eq (nth i buflst) 'x) (eq (nth (1+ i) buflst) '^))
				collect t))
		(if (or (null poly) (dummy-solve lst)) 1
			(let ((a 123) (b 123) (wrongness 0.0))
			(loop for i from 0 below (length buflst)
				when (eq (nth i buflst) '^)
					do (progn
						(setf a b)
						(setf b (nth (1+ i) buflst))
						(if (<= a b) (incf wrongness))))
			(/ wrongness 10.0)))))


;; A recursive function for adding brackets
(defun isolate (op lst)
	(let ((poly '(nil)) ind)
		; buffer the list with head and tail nils
		(setf lst (cons nil (reverse 
			(cons nil (reverse lst)))))

		(setf ind (position op lst))
		(if (integerp ind) 
			(setf poly (loop for i from 1 below (1- (length lst))
				when (eq i ind)
					collect (list (nth (1- i) lst) (nth i lst) (nth (1+ i) lst))
				when (not (or (eq i (1- ind)) (eq i ind) (eq i (1+ ind))))
					collect (nth i lst))))

		(if (find op poly) (isolate op poly) poly)))

; Function to apply BODMAS (well, just OM)
(defun bodmas (lst)
	(isolate '* (isolate '^ lst)))

;; A function for evaluating a possible 
;; polynomial given the target solution.
(defun evaluate (lst)
	(let ((a 'b) out)
		(setf out (get-nth 0 lst))
		(if (consp out) (setf out (evaluate out)))
		(loop for i from 0 below (- (length lst) 2)
			do (let ((lval out) (op (get-nth (1+ i) lst)) (rval (get-nth (+ 2 i) lst)))
				(if (consp rval) (setf rval (evaluate rval)))
				(setf out (funcall op lval rval))
				(incf i)))
		out))

;; A function to get the fitness value
;; 0 being the fittest (a solution)
(defun get-fitness (lst)
	(if (or (null lst) (integerp (position nil lst)))
		most-positive-short-float
		(handler-case
			(abs (complex (evaluate lst) (check-poly lst)))
		(arithmetic-error (ae) most-positive-short-float)
		(:no-error (ae) ae))))

;; For converting symbols, numbers 
;; and other things to string
(defun any-to-string (thing)
	(cond
		((symbolp thing) (symbol-name thing))
		((integerp thing) (write-to-string thing))
		(t (string thing))))

;; A function for printing the nice version of
;; equation (without parenthesis, with spacing)
(defun pretty-pheno (lst)
	(let ((out (flatten lst)) (outStr ""))
		(loop for i from 0 below (length out)
			do (setf outStr (concatenate 'string outStr 
				(if (find (nth i out) (list '- '+ '*)) 
					(concatenate 'string " " (any-to-string (nth i out)) " ")
					(any-to-string (nth i out))))))
		outStr))
