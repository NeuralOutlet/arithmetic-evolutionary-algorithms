# Case Study: Polynomials #

To compare arithmutation in situations where it should and shouldn't be useful we can use "f(x) = 0" where f is some polynomial. A genetic program can evolve f for a given x and a genetic algorithm can evolve x for a given f.

The GP version gives a problem that is detached from number-spaces. The GA version looks directly at numeric function optimisation where mutating the genome has a memetic relationship with the value it represents. Though in a GP the mutations may effect a value or a function or the structure of the program being evolved.

### Shared code ###

For a stronger comparison between the AGA and AGP the two programs share as much code as possible. 