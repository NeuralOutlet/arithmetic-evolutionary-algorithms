;; Population related functions

;; Create a new random population
(defun generate-pop (pop-size genome-size)
	(loop for i from 0 below pop-size
		collect (loop for j from 0 below genome-size
			collect (nth (random 2) '(0 1)))))
			
;; == Really nice macro for pop-nth ==
(defun remove-nth (list n)
  (remove-if (constantly t) list :start n :end (1+ n)))

(define-modify-macro remove-nth-f (n) 
	remove-nth "Remove the nth element")

(defmacro pop-nth (n lst)
  (let ((n-var (gensym)))
    `(let ((,n-var ,n))
       (prog1 (nth ,n-var ,lst)
         (remove-nth-f ,lst ,n-var)))))
;; ====================================	
 
;; Selection (tournament - dueling)
(defun selection (population)
	(let ((outpop '()) index-one index-two genome-one genome-two)
		(loop for i from 0 below (/ (length population) 5)
			do (progn
				(setf index-one (random (length population)))
				(setf genome-one (pop-nth index-one population))
				(setf index-two (random (length population)))
				(setf genome-two (pop-nth index-two population))
				(if (< (get-fitness (get-phenotype genome-one)) (get-fitness (get-phenotype genome-two)))
					(setf outpop (cons genome-one outpop))
					(setf outpop (cons genome-two outpop)))))
		outpop))


;; Function to apply crossover to 
;; the whole population
(defun pop-crossover (selected-pop size)
	(let ((out (loop for i from (length selected-pop) below size
		collect (crossover (nth (random 20) selected-pop) (nth (random 20) selected-pop)))))
	(append selected-pop out)))

;; Function to apply mutation to
;; the whole population
(defun pop-mutate (population &optional (type "flip-bit"))
	(let ((out population))
		(setf out (loop for i from 0 below (length out)
			collect (mutate (nth i out) (length out))))))
