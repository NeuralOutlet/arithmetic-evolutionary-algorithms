# Case Study: Roman Numerals #

The Python AGA is a fun endevour into the idea of number systems and GAs, instead of using a positional number system like in the C++ & Lisp versions, here we use the Roman Numerals with their symbol-value system (though it has some quasi-positional elements).

## Why Use Roman Numerals? ##

It's not particularly about RNs, it's about non-positional and quasi-positional systems - RNs are just an accessible place to start. Although, saying that, I couldn't find a problem that idealy fit RNs so I will put it through some different ones: 
* [Combinatorial Optimisation - The Knapsack Problem](https://en.wikipedia.org/wiki/Knapsack_problem)
* [Function Optimisation - Booth's Function](http://www.sfu.ca/~ssurjano/booth.html)


### Comparisons ###

RNs and their optional variations can be tested against Base 8 (0-7: Z,I,V,X,L,C,D,M). Also might add Base 5 & Base 10 in relation to the RN symbol base values.