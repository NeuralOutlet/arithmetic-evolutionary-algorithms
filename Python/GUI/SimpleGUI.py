
import sys, time, os

from PyQt4.QtGui import *
from PyQt4 import QtCore

from mainwindow import *

# Start the QApplication
a = QApplication(sys.argv)

window = MainWindow("SimpleGUI")
window.show()

# execute the QApplication
sys.exit(a.exec_())
