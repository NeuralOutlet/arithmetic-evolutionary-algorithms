
import sys, time, os

from PyQt4.QtGui import *
from PyQt4 import QtCore

class MainWindow(QMainWindow):
	# cstor
	def __init__(self, title, parent=None):
		super(MainWindow, self).__init__(parent)
		self.setWindowTitle(title)
		
		# Set class variables
		self.terminal = QTextEdit("So this is that const string")
		self.genCount = QLabel("Gen:  ")
		self.bestSolution = QLabel("Best Solution: ")
		
		# local stuff
		mainLay = QGridLayout()
		mainWidget = QWidget()
		boxLay = QGridLayout()
		box = QGroupBox("Output")
		boxLay.addWidget(self.terminal, 0,0,2,2, QtCore.Qt.AlignCenter)
		box.setLayout(boxLay)
		
		mainLay.addWidget(box, 0,0,1,2)
		mainLay.addWidget(self.genCount, 1,0,1,1, QtCore.Qt.AlignCenter)
		mainLay.addWidget(self.bestSolution, 1,1,1,1, QtCore.Qt.AlignCenter)
		mainWidget.setLayout(mainLay)
		self.setCentralWidget(mainWidget)