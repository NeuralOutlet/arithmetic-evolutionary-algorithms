
# This will be a class for representing
# Roman Numerals and Octal (Base 8) in union.
#
# The 'Roman Union' will have a base-state
# of Roman or Octal and use the respective
# operations such as +, -, and *.
class Base:
	Roman = 0
	Octal = 8

# For this we wil assign a relationship
# between the two symbol systems
class Symbols:
	def __init__(self):
		pass

	def __getitem__(self, key):
		if type(key) is int:
			return self.oct_to_rmn[key]
		if type(key) is str:
			return self.rmn_to_oct[key]
		return "Error"

	oct_to_rmn = {1:'I', 2:'V', 3:'X', 4:'L', 5:'C', 6:'D', 7:'M', 0:'Z'}
	rmn_to_oct = {'I':1, 'V':2, 'X':3, 'L':4, 'C':5, 'D':6, 'M':7, 'Z':0}

# Global bidirectional map for symbols
symbols = Symbols()
symbol_value = {'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000, 'Z':0}

# Values in Romanoctal is a list
class Romanoctal:
	data = []
	base = -1

	def __init__(self, value):
		# Construct value_list
		if isinstance(value, int):
			value_list = [int(x) for x in str(int(oct(value)))]
		elif isinstance(value, str):
			value_list = [x for x in value]
		else:
			value_list = value

		if type(value_list[0]) is int:
			self.base = Base.Octal
		elif type(value_list[0]) is str:
			self.base = Base.Roman
		else:
			print "Error"
		self.data = value_list


	# Convert symbols if base changed
	def to_base(self, base):
		if self.base is not base:
			for i, v in enumerate(self.data):
				self.data[i] = symbols[v]
		self.base = base
		return self

	# Print class instance as string of values
	def __str__(self):
		if self.base is Base.Octal:
			return ''.join(str(v) for v in self.data)
		if self.base is Base.Roman:
			return ''.join(self.data)
		return "Error"

	def minimise(self):
		vals = list(reversed(self.data))
		numeral = vals[0]
		count = 1
		tmp = ""
		for i in range(0, len(vals)):
			v = vals[i]
			print "tmp: " + tmp + ", i = " + str(i) + ", numeral = " + str(numeral) + ", v = " + str(v)
			if v != numeral: count -= 1
			nkey = symbols[numeral]
			if nkey % 2:
				if count >= 5:
					while count >= 5:
						count -= 5
						i+=5
						tmp += symbols[nkey+1]
				else:
					tmp += symbols[nkey]
			else:
				while count >= 2:
					count -= 2
					i+=2
					tmp += symbols[nkey+1]
				for j in range(0, count):
					tmp += symbols[nkey]

			count += 1
			numeral = v
		return list(reversed(v+tmp))

	def maximise(self):
		temp = ""
		data = str(self)
		end = len(data)-1
		for i in range(0, end):
			if data[i] == 'I':
				if data[i+1] == 'V':
					temp += 'IIII'
					i=i+1
				elif data[i+1] == 'X':
					temp += 'VIIII'
					i=i+1
				else:
					temp += 'I'
			elif data[i] == 'X':
				if data[i+1] == 'L':
					temp += 'XXXX'
					i=i+1
				elif data[i+1] == 'C':
					temp += 'LXXXX'
					i=i+1
				else:
					temp += 'X'
			elif data[i] == 'C':
				if data[i+1] == 'M':
					temp += 'DCCCC'
					i=i+1
				elif data[i+1] == 'D':
					temp += 'CCCC'
					i=i+1
				else:
					temp += 'C'
			else:
				temp += data[i]
		temp += data[end]
		return Romanoctal(temp)

	def order(self, roman):
		return sorted(roman, key=lambda x:symbols[x[0]], reverse=True)

	def roman_from_dec(self, rsum):
		out = []
		count = rsum
		for i in range(0, 7):
			while count >= symbol_value[symbols[7-i]]:
				numeral = symbols[7-i]
				out += [numeral]
				count -= symbol_value[numeral]
		return out

	def sub(self, roman):
		count = 0
		outlst = []
		for n in roman:
			count = count + symbol_value[n]
		return self.roman_from_dec(count)

	# Return the decimal value
	def get_value(self):
		power = len(self.data) - 1
		out = 0

		# Octal positional system
		if self.base is Base.Octal:
			for i, x in enumerate(self.data):
				out += x * (8 ** (power-i))

		# Roman numerals
		if self.base is Base.Roman:
			self.maximise()
			rmn = self.data #expand(self.data)
			for n in rmn:
				out += symbol_value[n]

		return out

	def padzeros(self, lhsd, rhsd):
		if self.base is Base.Octal: Z = 0
		if self.base is Base.Roman: Z = 'Z'
		lextra = max(0, (len(rhsd) - len(lhsd)))
		rextra = max(0, (len(lhsd) - len(rhsd)))
		return lhsd + [Z] * lextra, rhsd + [Z] * rextra


	# + operator
	def __add__(self, rhs):
		# Conform number systems
		if rhs.base is not self.base:
			rhs.to_base(self.base)
		out = []
		
		# Octal positional system
		if self.base is Base.Octal:
			carry = 0
			lhsd, rhsd = self.padzeros(list(reversed(self.data)), list(reversed(rhs.data)))
			for l,r in zip(lhsd, rhsd):
				unit = l + r + carry
				if unit >= self.base: 
					unit -= self.base
					carry = 1
				else: carry = 0
				out += [unit]
			if carry == 1: out += [1]
			out = list(reversed(out))

		# Roman numerls
		if self.base is Base.Roman:
			# expand
			l = self.maximise().data
			r = rhs.maximise().data
			# concat & order
			out = self.order(l+r)
			# substirute
			out = self.minimise()

		# Create a new union from list
		return Romanoctal(out)

def demo(test):
	testIn = Romanoctal(test)
	testOut = Romanoctal(testIn.minimise())
	testEnd = testOut.maximise()
	print "From Dec\t==> Minimised\t==> Maximised"
	print str(testIn) + " (" + str(testIn.get_value()) + ")\t==> " + str(testOut) + " (" + str(testOut.get_value()) + ")\t==> " + str(testEnd) + " (" + str(testEnd.get_value()) + ")"

# Test program
def main():
	roman = Romanoctal("IIII")
	octal = Romanoctal(4)
	bit = Romanoctal(1)

	for i in range(3, 7):
		print str(i+4) + ": " + str(roman) + " (" + str(roman.get_value()) + ")"
		roman = roman + bit
		octal = octal + bit

if __name__ == "__main__":
	main()

