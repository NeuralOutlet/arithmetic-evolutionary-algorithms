# Arithmetic Evolutionary Algorithms

There are two types I am working with, the original topic was Arithmetic Genetic Algorithms (AGAs) and the other is Arithmetic Genetic Programming (AGP) which is a side project. The original thesis on AGAs can be found [here](https://www.researchgate.net/publication/265600452_Comparing_Binary_Systems_in_Genetic_Algorithm_Encoding)


### What is this repository for? ###

* C++ code
    * The number system types
    * A full AGA framework
    * Test cases
    * Qt GUI
* Lisp code
    * AGP: Evolving polynomials for an indeterminate
    * AGA: Evolving indeterminates for a polynomial
    * Qt GUI
* Python code
    * Roman Numeral AGA
    * Qt GUI
* SETL code
    * ??? 
* SIN code
    * Awaiting a working version of SIN
