#include <iostream>

#include "Romansystem.h"

using namespace std;

int main()
{
	Romansystem r1("XXXXXXXVIII");
	Romansystem r2("CXXIII");
	cout << r1 << " + " << r2 << " = " << r1 + r2 << endl;
	cout << r1.GetValue() << " + " << r2.GetValue() << " = " << (r1 + r2).GetValue() << endl;
	cin.get();

	Romansystem r3("I");
	Romansystem eye("I");
	for(int i = 0; 0 < 100; ++i)
	{
		cout << r3 << " (" << r3.GetValue() << ")\n";
		r3 = r3 + eye;
	}

	return 0;
}