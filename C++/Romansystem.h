#include <iostream>
#include <string>

//Lets try this out
typedef std::string String;

class Romansystem
{
public:
	//Constructors
	Romansystem(){};
	Romansystem(String str);
	Romansystem(int val) {}
	Romansystem(bool isEmpty, int length);

	~Romansystem(){
		if( p_Roman )
			delete [] p_Roman;
	}

	//Accessors
	int GetValue();
	String GetString();

	Romansystem operator+(Romansystem& RHS);
	//Romansystem operator-(Romansystem RHS);

	friend std::ostream& operator<<( std::ostream& os, Romansystem& val);

private:
	String GetMaximal();
	void Minimize();
	void Order();

	char *p_Roman;
	int m_Length;
};