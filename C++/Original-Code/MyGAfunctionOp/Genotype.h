#include <iostream>
#include <vector>
#include <ctime>
#include <bitset>

#include "AltBitString.h"

using namespace std;

enum EncodingType{
	BIN = 0, GRAY, NEGA, SQRT, DRAGON, SUPER, GOLDEN, FIB, TRIB, IRRATIONAL_COMPLEX
};

struct Point{
	float x;
	float y;
};

class Genotype
{
public:
	Genotype(){};
	~Genotype(){};

	void SetChromosome( bool genes[64], double radix );
	void GetChromosome( bool temp[64] );

	bool GetGene( int index ){ return p_Genes[index]; };
	void SetGene( bool val, int index ){ p_Genes[index] = val; };
	Point GetPhenotype();
	Point GetLogPhenotype(int logN);
	void SetRadix( double r ){ p_Radix = r; };
	double GetRadix(){ return p_Radix; }
	float GetFitness();
	void SetFitness(float fit);

private:
	double p_Radix;
	bool p_Genes[64];
	float p_Fitness;
};