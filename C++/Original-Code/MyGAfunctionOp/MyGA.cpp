#include "MyGA.h"

double GeneticAlgo::Evaluate()
{
	Point bestPheno = p_Best; //bestPheno.x=2; bestPheno.y=-1;
	double best = p_Population.EvaluatePopulation( bestPheno );		

	if ( best < p_BestFitness )
	{
		p_BestFitness = best;
		p_Best = bestPheno;
	}

	return p_BestFitness;
}

void GeneticAlgo::Run()
{
	int count = 0;
	for ( int i = 0; i < p_MaxGenerations; i++ )
	{		
		Point p = p_Best;
		double bestFit = Evaluate();
		Select();
		Crossover();
		Mutate();
		int prev = p_Log.size();
		
		//if(i >= 9900)//p.x != p_Best.x || p.y != p_Best.y || i == 0 || i == p_MaxGenerations-1)
		//{
			cout << i << "\tx: " << p_Best.x << " y: " << p_Best.y << "\t\tFitness: " << bestFit << endl;
			PhenoFit log; log.fitness = bestFit; log.p = p_Best; log.generation = i;
			p_Log.push_back( log );	
		//}

		//if(bestFit == 0)
			//break;
	}
}

void GeneticAlgo::Crossover()
{
	for ( int i = 0; i < p_PopulationSize; i++ )
	{
		int r = rand() % 100;

		if ( r < p_CrossoverRate )
		{
			// Select random pair for crossover
			int indexA = rand() % p_PopulationSize;
			int indexB = rand() % p_PopulationSize;
			
			while ( indexA == indexB )
			{
				indexB = rand() % p_PopulationSize;
			}

			int cross = rand() % 64;
			p_Population.Crossover(indexA, indexB, cross);
		}
	}
}

void GeneticAlgo::Mutate()
{
	for ( int i = 0; i < p_PopulationSize; i++ )
	{
		int r = rand() % 100;

		if ( r < p_MutationRate )
		{
			p_Population.Mutate( i );
		}
	}
}

void GeneticAlgo::Select()
{
	// For each pair of chromosomes selected
	int i = 0;

	while ( i < p_PopulationSize/4 )
	{	
		// Get the chromosome pair for tournament selection
		int index1 = rand() % p_PopulationSize;
		int index2 = rand() % p_PopulationSize;

		while ( index1 == index2 )
		{
			index2 = rand() % p_PopulationSize;
		}

		double fitness1 = fabs( p_Population.GetGenotypeFit( index1 ) );
		double fitness2 = fabs( p_Population.GetGenotypeFit( index2 ) );				
		
		if ( fitness1 > fitness2 )
		{
			// Copy chromosome 1 elements into chromosome 2
			p_Population.CopyChromosome( index2, index1 );			
		}
		else
		{
			// Copy chromosome 2 elements into chromosome 1
			p_Population.CopyChromosome( index1, index2 );			
		}

		i++;
	}
}

void GeneticAlgo::SetParameters( EncodingType eType, int popultionSize, int crossoverRate, int mutationRate, int maxGenerations, int searchSpace )
{
	p_Encoding = eType;
	p_PopulationSize = popultionSize;
	p_CrossoverRate = crossoverRate;
	p_MutationRate  = mutationRate;
	p_MaxGenerations = maxGenerations;
	p_SearchSpace = searchSpace;
	p_BestFitness = 0xFFFFFFFF;	
}

