
#include "Genotype.h"

class Population
{
public:
	Population(){};
	~Population();

	void Initialise( int popSize, EncodingType e, int searchSize );

	void Crossover( int indexA, int indexB, int crossPoint );
	void Mutate( int mutePoint );

	float GetGenotypeFit( int index );
	float GetPopulationFit( int index );
	double EvaluatePopulation( Point& bestest );
	Genotype* CreateRandomChromosome();
	//Used during selection
	void CopyChromosome( int source, int dest );
	double PosRadixOf(EncodingType e);

private:
	double CalculateFitnessFunction( Point val );
	void ArithmutatePhi(int values[64], int index);
	void Standardise(int vals[64]);
	void ArithmutateBin(int values[64], int index);
	vector<Genotype*> p_Population;
	int p_PopSize;
	int p_SearchSpaceSize;
};