
#include "Genotype.h"


void Genotype::SetChromosome( bool genes[64], double radix )
{
	p_Radix = radix;
	for(int i = 0; i < 64; ++i)
		p_Genes[i] = genes[i];
}

void Genotype::GetChromosome(bool temp[64] )
{
	for(int i = 0; i < 64; ++i)
		temp[i] = p_Genes[i];
}

float Genotype::GetFitness(){ return p_Fitness; };
void Genotype::SetFitness(float fit){ p_Fitness = fit; };

Point Genotype::GetLogPhenotype(int N)
{
	Point p;
	if(p_Radix == 0)
	{
		complex<double> p_CRadix(-1, 1);
		int MSD = 8;

		complex<double> xInteger = 0.0;
		for(int i = 0; i < MSD; ++i)
		{
			complex<double> digit( p_Genes[i], 0);
			xInteger += digit * pow( p_CRadix, MSD-i );
		}

		complex<double> xExpansion = 0.0;
		for(int i = MSD; i < 32; ++i)
		{
			complex<double> digit( p_Genes[i], 0);
			xExpansion += digit * pow(p_CRadix, -i);
		}

		complex<double> yInteger = 0.0;
		for(int i = 0; i < MSD; ++i)
		{
			complex<double> digit( p_Genes[i+32], 0);
			yInteger += digit * pow( p_CRadix, MSD-i );
		}

		complex<double> yExpansion = 0.0;
		for(int i = MSD; i < 32; ++i)
		{
			complex<double> digit( p_Genes[i+32], 0);
			yExpansion += digit * pow(p_CRadix, -i);
		}

	 
		p.x = abs((xInteger + xExpansion));
		p.y = abs((yInteger + yExpansion));
	}
	else
	{
		int MSD = log((double)N)/log(p_Radix);
		int length = MSD + 20;
		int xSign = (p_Genes[0]) ? -1 : 1;
		int ySign = (p_Genes[32]) ? -1 : 1;
	
		double xInteger = 0.0;
		for(int i = 0; i < MSD+1; ++i)
			xInteger += p_Genes[i+1] * pow( p_Radix, MSD-i );

		double xExpansion = 0.0;
		for(int i = MSD; i < length; ++i)
			xExpansion += p_Genes[i+1] * pow(p_Radix, -i);

		double yInteger = 0.0;
		for(int i = 0; i < MSD+1; ++i)
			yInteger += p_Genes[i+33] * pow( p_Radix, MSD-i );

		double yExpansion = 0.0;
		for(int i = MSD; i < length; ++i)
			yExpansion += p_Genes[i+33] * pow(p_Radix, -i);

	 
		p.x = xSign * (xInteger + xExpansion);
		p.y = ySign * (yInteger + yExpansion);
	}
	return p;
}

Point Genotype::GetPhenotype()
{
	Point p;

	if(p_Radix == 0)
	{
		bool temp[64];
		AltBinary both(complex<double>(-1,1), temp);
		for (int i = 0; i < 64; ++i)
			temp[i] = p_Genes[i];
		both = temp;
		p.x = both.ToCDouble().real();
		p.y = both.ToCDouble().imag();
	}
	else
	{
		bool temp[32];
		AltBinary x(p_Radix, temp);
		for (int i = 0; i < 32; ++i)
			temp[i] = p_Genes[i];
		x = temp;

		AltBinary y(p_Radix, temp);
		for (int i = 32; i < 64; ++i)
			temp[i-32] = p_Genes[i];
		y = temp;

		p.x = x.ToFloat();
		p.y = y.ToFloat();
	}

	return p;
}