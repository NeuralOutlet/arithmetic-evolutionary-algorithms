#include <iostream>
#include <fstream>
#include <Windows.h>

#include "ColourMapping.h"
#include "MyGA.h"


using namespace std;

int main()
{
	const int sampleSize = 1000; //amount of times GA is repeated
	vector<PhenoFit> tempLogs;

	ofstream myfileFit;

	GeneticAlgo ga[sampleSize];
	for(int i = 0; i < sampleSize; ++i)
	{
		ga[i].SetParameters(GOLDEN, 100, 80, 50, 10000, NULL);
		ga[i].Initialise();
		ga[i].Run();
		tempLogs = ga[i].GetLog();

		myfileFit.open ("10000 gens R1 GOLDEN SandA.txt", std::ofstream::out | std::ofstream::app);

		for(int j = 0; j < tempLogs.size(); ++j)
		{
			myfileFit << tempLogs[j].fitness << ", ";
		}	
		myfileFit << endl;		myfileFit.close();
	}

	cin.get();

	//============================
	//Displaying the plane contor
	//============================
	HDC hdc = GetDC(NULL);
	#define eConst         2.71828182845904523536
	#define piConst        3.14159265358979323846
	//Ackley's Function ---------- global min at f(0,0) = 0
	for(int i = -250; i < 250; ++i)
	{
		for(int j = -250; j < 250; ++j)
		{
			//In the form (MAX colour,  i or j * scale) 
			//Ackley: (14, 0.02), 
			//Rosenbrock: (2509, 0.025)
			//Himmelblau: (890, 0.025)

			double x = (double)i/50;
			double y = (double)j/50;

			#define phi ((1+sqrt(5.0))/2)
			// Rosenbrock's Valley --------- global min at f(1,1) = 0
			double colour = ( pow( (double)( phi - x ), 2 ) ) + 
		       100 * ( pow( (double) ( y - ( x * x ) ), 2 ) ) ;	
			
			//Ackley function
			//double colour = -20 * pow( eConst, (-0.2 * sqrt( 0.5 * ((pow(x, 2) + (pow(y, 2)) ) ))))  //Dip to minimum
			//			- pow(eConst, 0.5 * (cos(2*piConst*x) + cos(2*piConst*y))) + 20 + eConst; //periodic wave shapes

			//Himmelblau function
			//double colour = pow( pow(x, 2) + y - 11, 2) + pow(x+pow(y,2) -7, 2);

			
			//new func ( (x+y*((1+sqrt(5))/2) )^2 +  (x+sqrt(5))^2
			//double colour = pow(x + y * phi, 2) + pow(x + sqrt(5.0), 2);

			COLOUR col = GetColour(colour, 0, 30); 
			COLORREF c2 = RGB(col.r*255,col.g*255,col.b*255);

			SetPixel(hdc, i+350, j+350, c2);
		}
	}

	//corner:
	DrawSquare(hdc, (5*50)+350, (5*50)+350, 30, RGB(255,255,0));

	float x =  50*-sqrt(5.0);
	float y =  50*((5+sqrt(5.0))/2);

	DrawSquare(hdc, x+350, y+350, 15, RGB(0,0,0));
	DrawSquare(hdc, x+350, y+350, 10, RGB(255,255,255));

	cin.get();
	//============================

	return 0;
}