#include "AltBitString.h"

AltBinary::AltBinary(double radix, bool value[32])
{
	for(int i = 0; i < 32; ++i)
		p_BitString[i] = value[i];

	for(int i = 32; i < 64; ++i)
		p_BitString[i] = 0;

	p_M = 23;
	if(radix < 0.0)
	{
		p_Type = NEGATIVE; 
		p_E = 8; //extra long exponent when no sign bit is needed
	}
	else
	{
		p_Type = POSITIVE; 
		p_E = 8;
	}

	p_Base = complex<double>(radix, 0);
}

AltBinary::AltBinary(complex<double> radix, bool value[64])
{
	for(int i = 0; i < 64; ++i)
		p_BitString[i] = value[i];

	p_M = 53;
	p_E = 11;

	p_Base = radix;
}

AltBinary AltBinary::operator=(bool val[32])
{
	for(int i = 0; i < 32; ++i)
		p_BitString[i] = val[i];

	return *this;
}

float AltBinary::ToFloat()
{
	//Sign:
	double sign = 1;
	if(p_Type == POSITIVE)
		sign = pow((-1.0), p_BitString[31]);

	//Mantissa:
	double sum = 0;
	for(int i = 1; i < p_M; ++i)
		sum += p_BitString[p_M-i] * pow(p_Base.real(), -i);

	double mantissa = 1+sum;

	double exp = 0;

	for(int i = 0; i < p_E; ++i)
		exp += p_BitString[p_M+i] * pow(p_Base.real(), i);

	double	bias = 0;
	if(POSITIVE)
		bias = pow( p_Base.real(), (p_E-1) ) - 1;

	double exponent = pow(p_Base.real(), (double)(exp-bias) );
	
	
	double out = sign * mantissa * exponent;

	
	return out;
}

void AltBinary::BitsOut()
{
	//TEST
	for(int i = 0; i < 32; ++i)
		cout << p_BitString[31-i];
	cout << "\t=\t" << ToFloat() << endl;
}

complex<double> AltBinary::ToCDouble()
{
	//Mantissa
	complex<double> sum = 0;
	for(int i = 1; i < p_M; ++i)
		sum += complex<double>(p_BitString[p_M-i], 0) * pow(p_Base, -i);

	complex<double> mantissa = complex<double>(1.0,0.0) + sum;

	//Exponent
	complex<double> exp = 0;

	for(int i = 0; i < p_E; ++i)
		exp += complex<double>(p_BitString[p_M+i], 0) * pow(p_Base, i);

	complex<double> bias = pow( p_Base, (p_E-1)) - complex<double>(1.0,0.0);

	//Output
	complex<double> out = mantissa * pow(p_Base.real(), (exp-bias) );

	return out;
}