#include "Population.h"

struct PhenoFit{
	Point p;
	double fitness;
	int generation;
};

class GeneticAlgo
{
public:
	GeneticAlgo(){};
	~GeneticAlgo(){};

	void Initialise(){ p_BestFitness = 0xFFFFFFFF; p_Population.Initialise( p_PopulationSize, p_Encoding, p_SearchSpace ); };
	void SetParameters(
		EncodingType eType,
		int popultionSize,
		int crossoverRate,
		int mutationRate,
		int maxGenerations,
		int searchSpace );
	void Run();

	EncodingType GetEncoding(){return p_Encoding; };

	vector<PhenoFit> GetLog( ){ return p_Log; };

private:
	//void CreatePopulation();
	vector<PhenoFit> p_Log;
	double Evaluate();
	void Crossover();
	void Mutate();
	void Select();

	EncodingType p_Encoding;
	int p_MutationRate;
	int p_CrossoverRate;
	int p_PopulationSize;
	int p_MaxGenerations;
	int p_SearchSpace;

	int p_BestFitnessIndex;
	double p_BestFitness;
	Point p_Best;

	Population p_Population;
};
