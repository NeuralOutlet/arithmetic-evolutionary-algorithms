#include <complex>
#include <iostream>
#include <string>

using namespace std;

enum NumberSystem { POSITIVE = 0, NEGATIVE, COMPLEX };

class AltBinary
{
public:
	AltBinary(double radix, bool value[32]);
	AltBinary(complex<double> radix, bool value[64]);
	AltBinary operator=(string val){return *this;};
	AltBinary operator=(float val){return *this;};
	AltBinary operator=(bool val[32]);

	float ToFloat();
	string ToFloatString(){};
	string ToString(){};
	complex<double> ToCDouble();
	void BitsOut();

private:
	NumberSystem p_Type; //1= positive real, 2=negative real, 3=complex
	bool p_BitString[64]; //max value
	complex<double> p_Base;
	int p_M;
	int p_E;
};