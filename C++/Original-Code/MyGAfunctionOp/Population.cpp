
#include "Population.h"


Population::~Population()
{
	// Delete the chromosome pointers
	for ( int i = 0; i < p_PopSize; ++i )
	{
		Genotype* chr = p_Population.at( i );

		if ( chr )
		{
			delete chr;
		}
	}

	p_Population.clear();
}
Genotype* Population::CreateRandomChromosome()
{
	Genotype* chr = new Genotype();

	// Randomize chromosome elements	
	for ( int i = 0; i < 64; i++ )
	{		
		bool value = rand() % 100 < 50 ? 0 : 1;
		chr->SetGene( value, i );
	}

	return chr;
}

void Population::Initialise( int popSize, EncodingType e, int searchSize )
{
	srand(time(0));
	p_PopSize = popSize;
	p_SearchSpaceSize = searchSize;
	for(int i = 0; i < popSize; ++i)
	{
		Genotype *chromosome = CreateRandomChromosome();
		chromosome->SetRadix( PosRadixOf(e) );
		p_Population.push_back( chromosome );
	}
}

void Population::Crossover(int indexA, int indexB, int crossPoint )
{
	for(int i = crossPoint; i < 64; ++i)
	{
		bool temp = p_Population[indexA]->GetGene(i);
		p_Population[indexA]->SetGene( p_Population[indexB]->GetGene(i), i );
		p_Population[indexB]->SetGene( temp, i );
	}
}

void Population::Mutate( int index )
{
	int m = rand() % 64;
	
	int nums[64];
	for(int i = 0; i < 64; ++i)
		nums[i] = p_Population[index]->GetGene(i);

	if(p_Population[index]->GetRadix() < 2)
	{
		ArithmutatePhi(nums, m);
		Standardise(nums);

		for(int i = 0; i < 64; ++i)
			p_Population[index]->SetGene( nums[i], i);
	}
	else
	{
		p_Population[index]->SetGene( !p_Population[index]->GetGene(m), m );
		//ArithmutateBin(nums, m);
	}
}

void Population::ArithmutatePhi(int values[64], int index)
{
	//Initial mutation:
	if(values[index])
	{
		values[index] = 0;
		if(index-1 >= 0)
			++values[index-1];
		if(index+2 < 64)
			++values[index+2];
		//cout<< BinString(values) << endl;
	}
	else
	{
		values[index] = 1;
		//cout<< BinString(values) << endl;
		return;
	}
	
	//Carry from right to left, restarting everytime
	for(int i = 0; i <64; ++i)
	{
		if(values[63-i] > 1)
		{
			if(values[63] == 2)
			{
				values[63] = 0;
				values[62] += 1;
			}

			values[63-i] = 0;
			if(63-i-1 >= 0)
				++values[63-i-1];
			if(63-i+2 < 64)
				++values[63-i+2];
			i = 0;
			//cout<< BinString(values) <<  "\t" << values[63] << endl;
		}
	}	
}

void Population::Standardise(int vals[64])
{
	for(int i = 0; i < 64; ++i)
	{
		int pos = 63-i;

		//Overflow rule:
		if(vals[pos] > 1)
		{
			if(pos-1 >= 0)
				vals[pos-1] += 1;
			if(pos-2 >= 0)
				vals[pos+2] +=1;
			vals[pos]-=1;
			//i = 0;
		}
		//cout <<"O: "<< BinString(vals) << endl;

		//Standardise rule:
		if(pos-1 >= 0)
		{
			if(vals[pos] > 0 && vals[pos-1] > 0)
			{
				vals[pos] -= 1; 
				vals[pos-1] -= 1;
				if(pos-2>=0)
					vals[pos-2] += 1;
			}
		}
		//cout <<"S: "<< BinString(vals) << endl;
		
	}
}

void Population::ArithmutateBin(int values[64], int index)
{
	//Initial mutation:
	if(values[index])
	{
		values[index] = 0;
		if(index-1 >= 0)
			++values[index-1];
		//cout<< BinString(values) << endl;
	}
	else
	{
		values[index] = 1;
		//cout<< BinString(values) << endl;
		return;
	}
	

	for(int i = 0; i <64; ++i)
	{
		if(values[63-i] > 1)
		{
			values[63-i] = 0;
			if(63-i-1 >= 0)
				++values[63-i-1];

			//cout<< BinString(values) <<  "\t" << values[63] << endl;
		}
	}	
}

float Population::GetGenotypeFit( int index )
{
	Point p;
	//p = p_Population[index]->GetLogPhenotype(p_SearchSpaceSize);
	p = p_Population[index]->GetPhenotype();

	return CalculateFitnessFunction( p );
}

double Population::CalculateFitnessFunction( Point val )
{
	#define eConst         2.71828182845904523536
	#define piConst        3.14159265358979323846
	#define phiConst       ((1+sqrt(5.0))/2)

	// Rosenbrock's Valley --------- global min at f(1,1) = 0
	double fitness = ( pow( (double)( 1 - val.x ), 2 ) ) + 
		       100 * ( pow( (double) ( val.y - ( val.x * val.x ) ), 2 ) ) ;		

	//Bukin function N. 6 --------- global min at f(-10,1) = 0
	//double fitness = 100 * sqrt( abs( val.y - 0.01 * pow(val.x, 2) ) ) + 0.01 * abs( val.x + 10 );

	//Ackley's Function ---------- global min at f(0,0) = 0
	//double fitness = -20 * pow( eConst, (-0.2 * sqrt( 0.5 * ((pow(val.x, 2) + (pow(val.y, 2)) ) ))))  //Dip to minimum
	//			- pow(eConst, 0.5 * (cos(2*piConst*val.x) + cos(2*piConst*val.y))) + 20 + eConst; //periodic wave shapes

	//new function
	//double fitness = pow(val.x + val.y * phiConst, 2) + pow(val.x + sqrt(5.0), 2);

	return fitness;
}

void Population::CopyChromosome(int sourceIndex, int destinationIndex)
{
	// Copy elements and fitness of source chromosome 
	// into the destination chromosome
	for ( int i = 0; i < 64; i++ )
	{
		// Get source chromosome element
		bool value = p_Population[sourceIndex]->GetGene( i );

		// Write this element value into the destination element
		p_Population[destinationIndex]->SetGene( value, i );
	}

	// Set the destination chromosome fitness
	double fitness = p_Population[sourceIndex]->GetFitness();
	p_Population[destinationIndex]->SetFitness( fitness );
}

// Evaluate the population fitnesses
double Population::EvaluatePopulation( Point& bestest )
{
	double totalFitness = 0.0;
	double aveFitness = 0.0;
	double bestFitness = 0xFFFFFFFF;
	int bestFitnessIndex = 0;	

	for ( int i = 0; i < p_PopSize; i++ )
	{	
		double fitness = GetGenotypeFit( i );		
		p_Population[i]->SetFitness( fitness );

		totalFitness += fitness;

		// Store best solution
		if ( i == 0 ) bestFitness = fitness;

		if ( fitness < bestFitness )
		{
			bestFitness = fitness;
			bestFitnessIndex = i;
			//bestest = p_Population[i]->GetLogPhenotype(p_SearchSpaceSize);
			bestest = p_Population[i]->GetPhenotype();
		}
	}

	return bestFitness;
}

double Population::PosRadixOf(EncodingType e)
{
	double r;
	switch(e)
	{
	case BIN:
		r = 2;
		break;
	case SQRT:
		r = 1.414213562373095048801688724209698078569671875376948073176679;
	case GOLDEN:
		r = (1.0 + sqrt(5.0))/2.0;
		break;
	case TRIB:
		r = 1.83928675521416;
		break;
	case DRAGON:
		r = 0;
		break;
	default:
		r = NULL;
		break;
	}
	return r;
}