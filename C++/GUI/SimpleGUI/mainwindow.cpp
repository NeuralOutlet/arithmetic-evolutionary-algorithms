#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGridLayout>
#include <QGroupBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGridLayout *mainLay = new QGridLayout;
    QWidget *mainWidget = new QWidget;

    QGridLayout *boxLay = new QGridLayout;
    QGroupBox *box = new QGroupBox("Output");
    QTextEdit *terminal = new QTextEdit("So this is that const string");
    boxLay->addWidget(terminal, 0,0,2,2, Qt::AlignCenter);
    box->setLayout(boxLay);

    p_GenCount = new QLabel("Gen:  ");
    p_BestSolution = new QLabel("Best Solution: ");

    // Set it
    mainLay->addWidget(box, 0,0,1,2);
    mainLay->addWidget(p_GenCount, 1,0,1,1, Qt::AlignCenter);
    mainLay->addWidget(p_BestSolution, 1,1,1,1, Qt::AlignCenter);
    mainWidget->setLayout(mainLay);
    setCentralWidget(mainWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ChangeOptions()
{
    // Stuff
}
