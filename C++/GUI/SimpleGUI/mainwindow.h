#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QComboBox>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void ChangeOptions();

private:
    Ui::MainWindow *ui;

    QComboBox *p_ProblemType;
    QComboBox *p_SelectType;
    QComboBox *p_MutateType;
    QLabel *p_BestSolution;
    QTextEdit *p_Terminal;
    QLabel *p_GenCount;
};

#endif // MAINWINDOW_H
