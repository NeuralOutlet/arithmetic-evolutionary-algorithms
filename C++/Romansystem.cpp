
#include "Romansystem.h"
#include <vector>

//==========================
// Constructor from data
//==========================
Romansystem::Romansystem(bool isEmpty, int length)
{
	p_Roman = new char[length];
	m_Length = length;

	for(int i = 0; i < m_Length; ++i)
	{
		if(isEmpty)
			p_Roman[i] = 'Z';
		else
			p_Roman[i] = 'X'; //GenerateRoman();
	}
}

//==========================
// Constructor from string
//==========================
Romansystem::Romansystem(String rStr)
{
	int length = rStr.size();
	p_Roman = new char[length];
	m_Length = length;
	
	for(int i = 0; i < m_Length; ++i)
	{
		if(rStr[i] == 'U' || rStr[i] == 'S' || rStr[i] == 'I' 
			|| rStr[i] == 'V' || rStr[i] == 'X' || rStr[i] == 'L' 
			|| rStr[i] == 'C' || rStr[i] == 'D' || rStr[i] == 'M')
		{
			p_Roman[i] = rStr[i];
		}
		else
		{
			p_Roman[i] = 'Z';
		}
	}
}

//==========================
// Get the readable string
//==========================
String Romansystem::GetString()
{
	String temp = "";
	for(int i = 0; i < m_Length; ++i)
		if(p_Roman[i] != 'Z')
			temp += p_Roman[i];

	return temp;
}

//==========================
// Order the string
//==========================
void Romansystem::Order()
{
	String temp = "";
	int letterCount = 0;
	int end = m_Length;
	char nextVal = 'M';

	do
	{
		for(int i = 0; i < m_Length; ++i)
		{
			if(p_Roman[i] == nextVal)
			{
				temp += nextVal;
			}
		}

		if(nextVal == 'M')
			nextVal = 'D';
		else if(nextVal == 'D')
			nextVal = 'C';
		else if(nextVal == 'C')
			nextVal = 'L';
		else if(nextVal == 'L')
			nextVal = 'X';
		else if(nextVal == 'X')
			nextVal = 'V';
		else if(nextVal == 'V')
			nextVal = 'I';
		else if(nextVal == 'I')
			nextVal = 'S';
		else if(nextVal == 'S')
			nextVal = 'U';
		else{}

		++letterCount;

	}while(letterCount < 9);

	
	for(int i = 0; i < m_Length; ++i)
	{
		p_Roman[i] = temp[i];
	}
}

//==========================
// Get the expanded form of
// Roman numerals in string
//==========================
String Romansystem::GetMaximal()
{
	String temp = "";
	for(int i = 0; i < m_Length; ++i)
	{
		if(p_Roman[i] == 'I')
		{
			if(p_Roman[i+1] == 'V')
			{
				temp+= "IIII";
				++i;
			}
			else if(p_Roman[i+1] == 'X')
			{
				temp += "VIIII";
				++i;
			}
			else{
				temp += "I";
			}
		}
		else if(p_Roman[i] == 'X')
		{
			if(p_Roman[i+1] == 'L')
			{
				temp+= "XXXX";
				++i;
			}
			else if(p_Roman[i+1] == 'C')
			{
				temp += "LXXXX";
				++i;
			}
			else
				temp += "X";
		}
		else if(p_Roman[i] == 'C')
		{
			if(p_Roman[i+1] == 'M')
			{
				temp += "DCCCC";
				++i;
			}
			else if(p_Roman[i+1] == 'D')
			{
				temp += "CCCC";
				++i;
			}
			else
				temp += "C";
		}
		else
		{
			temp += p_Roman[i];
		}
	}
	return temp;
}

std::ostream& operator<<(std::ostream& os, Romansystem& val)
{
	os << val.GetString();
    return os;
}

Romansystem Romansystem::operator+(Romansystem& RHS)
{
	//Get longest terms
	String a = GetMaximal();
	String b = RHS.GetMaximal();

	//Concatenate
	String c = a + b;
	Romansystem *output = new Romansystem(c);

	//Order
	output->Order();

	//Minimalise the new number
	output->Minimize();

	return (*output);
}

char powerRank[8] = { 'I', 'V', 'X', 'L', 'C', 'D', 'M', 'M' }; //Second M for overflow
int GetPower(char rmn)
{
	if(rmn == 'I') return 0;
	if(rmn == 'V') return 1;
	if(rmn == 'X') return 2;
	if(rmn == 'L') return 3;
	if(rmn == 'C') return 4;
	if(rmn == 'D') return 5;
	if(rmn == 'M') return 6;
	return -1;
}

void Romansystem::Minimize()
{
	std::vector<String> groups;

	int numeralCount = 1;
	int numeralPower = GetPower( p_Roman[m_Length-1] );
	for(int i = m_Length-1; i > -2; --i) //working from the leftmost numeral
	{
		//When we enter a new group of numeral type:
		if( powerRank[numeralPower] != p_Roman[i] )
		{
			String temp = "";
			//if it is group I, X or C (or M)
			if( numeralPower % 2 == 0 )
			{
				while ( numeralCount > 5 )
				{
					numeralCount -= 5;
					temp += powerRank[ numeralPower+1 ];
				}

				if(numeralCount == 4)
				{
						temp += powerRank[numeralPower] + powerRank[numeralPower+1];
						numeralCount -= 4;
				}

				for(int i = 1; i < numeralCount; ++i)
				{
					temp += powerRank[numeralPower];
				}
			}
			else //if it is group V, L or D
			{
				while( numeralCount > 2 )
				{
					numeralCount -= 2;
					temp += powerRank[numeralPower+1];
				}

				for(int i = 1; i < numeralCount; ++i)
				{
					temp += powerRank[numeralPower];
				}
			}

			groups.push_back(temp);
			//Reset numeralCount
			numeralCount = 1;
			numeralPower = GetPower( p_Roman[i] );
		}

		//Increment numeralCount
		++numeralCount;
	}

	String temp = "";
	std::vector<String>::iterator iter = groups.begin();
	for(iter; iter != groups.end(); ++iter)
	{
		temp = (*iter) + temp;
	}

	//Reallocate memory:
	m_Length = temp.size();
	delete [] p_Roman;
	p_Roman = new char[m_Length];
	for(int i = 0; i < m_Length; ++i)
		p_Roman[i] = temp[i];
}

int Romansystem::GetValue()
	{
	int temp = 0;
	for(int i = 0; i < m_Length; ++i)
	{
		if(p_Roman[i] == 'U')
			temp += (1.0/12.0);
		if(p_Roman[i] == 'S')
			temp += (6.0/12.0);

		if(p_Roman[i] == 'I')
		{
			if(p_Roman[i+1] == 'V')
			{
				temp+= 4;
				++i;
			}
			else if(p_Roman[i+1] == 'X')
			{
				temp += 9;
				++i;
			}
			else{
				temp += 1;
			}
		}
		else if(p_Roman[i] == 'V')
			temp += 5;
		else if(p_Roman[i] == 'X')
		{
			if(p_Roman[i+1] == 'L')
			{
				temp+= 40;
				++i;
			}
			else if(p_Roman[i+1] == 'C')
			{
				temp += 90;
				++i;
			}
			else
				temp += 10;
		}
		else if(p_Roman[i] == 'L')
			temp += 50;
		else if(p_Roman[i] == 'C')
		{
			if(p_Roman[i+1] == 'M')
			{
				temp += 900;
				++i;
			}
			else if(p_Roman[i+1] == 'D')
			{
				temp += 400;
				++i;
			}
			else
				temp += 100;
		}
		else if(p_Roman[i] == 'D')
			temp += 500;
		else if(p_Roman[i] == 'M')
			temp += 1000;
		else {}
	}

	return temp;
}
